﻿1
00:00:00,000 --> 00:00:04,264
Neste breve vídeo, mostraremos como criar um modelo de relatório

2
00:00:04,264 --> 00:00:06,686
no Gerenciador de Anúncios do Facebook para Excel

3
00:00:06,686 --> 00:00:09,767
para que você possa extrair dados de desempenho das suas contas de anúncios.

4
00:00:11,267 --> 00:00:15,172
Para criar um modelo de relatório,
clique em "Gerenciar modelos".

5
00:00:16,307 --> 00:00:17,965
Clique em "Criar modelo"

6
00:00:17,965 --> 00:00:21,280
e personalize a aparência do relatório como preferir

7
00:00:21,280 --> 00:00:24,051
no Gerenciador de Anúncios do Facebook para Excel.

8
00:00:24,051 --> 00:00:26,795
Considere as métricas mais importantes para você

9
00:00:26,795 --> 00:00:30,376
e estruture o modelo de relatório de acordo com elas.

10
00:00:31,922 --> 00:00:33,855
Clique em "Criar".

11
00:00:33,855 --> 00:00:35,862
Agora que o modelo de relatório está pronto,

12
00:00:35,862 --> 00:00:38,533
você pode baixar relatórios das suas contas de anúncios.

13
00:00:40,417 --> 00:00:43,545
Saiba mais sobre o Gerenciador de Anúncios do Facebook para Excel

14
00:00:43,545 --> 00:00:45,338
nas nossas páginas de ajuda.


