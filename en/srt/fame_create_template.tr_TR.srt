﻿1
00:00:00,618 --> 00:00:01,858
Bu kısa videoda,

2
00:00:01,858 --> 00:00:04,210
reklam hesaplarınızdan performans verilerinizi almak için

3
00:00:04,210 --> 00:00:06,715
Excel için Facebook Reklam Yöneticisi'nde bir rapor şablonunu

4
00:00:06,715 --> 00:00:09,834
nasıl oluşturabileceğinizi göstereceğiz.

5
00:00:11,123 --> 00:00:15,201
Bir rapor şablonu oluşturmak için, Şablonları Yönet'e tıklayın.

6
00:00:16,316 --> 00:00:18,198
Şablon Oluştur'a tıklayın

7
00:00:18,198 --> 00:00:21,216
ve raporunuzun Excel için Facebook Reklam Yöneticisi'nde

8
00:00:21,216 --> 00:00:24,290
görünmesini istediğiniz şekilde şablonu özelleştirin.

9
00:00:24,290 --> 00:00:27,270
En çok ilginizi çeken ölçümleri göz önünde bulundurmalı ve

10
00:00:27,270 --> 00:00:30,480
rapor şablonunuzu bu ölçümlere dayalı olarak yapılandırmalısınız.

11
00:00:31,967 --> 00:00:32,929
Oluştur'a tıklayın.

12
00:00:33,954 --> 00:00:35,916
Rapor şablonunu oluşturduktan sonra

13
00:00:35,916 --> 00:00:38,603
reklam hesaplarınızdan raporları indirebilirsiniz.

14
00:00:40,448 --> 00:00:43,590
Excel için Facebook Reklam Yöneticisi hakkında daha fazla bilgi için

15
00:00:43,590 --> 00:00:45,168
yardım sayfalarımızı ziyaret edin.

