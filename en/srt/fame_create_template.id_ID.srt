﻿1
00:00:00,000 --> 00:00:04,264
Dalam video pendek ini, kami akan menunjukkan cara membuat templat laporan

2
00:00:04,264 --> 00:00:06,686
di Pengelola Iklan Facebook untuk Excel

3
00:00:06,686 --> 00:00:09,767
sehingga Anda dapat menggunakan data kinerja dari akun iklan Anda.

4
00:00:11,267 --> 00:00:15,172
Untuk membuat templat laporan,
klik 'Kelola Templat.'

5
00:00:16,307 --> 00:00:17,965
Klik 'Buat Templat'

6
00:00:17,965 --> 00:00:21,280
lalu sesuaikan tampilan laporan seperti keinginan Anda

7
00:00:21,280 --> 00:00:24,051
di Pengelola Iklan Facebook untuk Excel.

8
00:00:24,051 --> 00:00:26,795
Anda harus memikirkan metrik yang paling Anda minati

9
00:00:26,795 --> 00:00:30,376
lalu menyusun templat laporan Anda seputar metrik tersebut.

10
00:00:31,922 --> 00:00:33,855
Klik 'Buat.'

11
00:00:33,855 --> 00:00:35,862
Setelah Anda membuat templat laporan,

12
00:00:35,862 --> 00:00:38,533
Anda dapat mengunduh laporan dari akun iklan Anda.

13
00:00:40,417 --> 00:00:43,545
Untuk mempelajari selengkapnya tentang Pengelola Iklan Facebook untuk Excel,

14
00:00:43,545 --> 00:00:45,338
kunjungi halaman bantuan kami.


