﻿1
00:00:00,000 --> 00:00:04,264
En este breve video, te mostraremos
cómo crear una plantilla de informe

2
00:00:04,264 --> 00:00:06,686
en el administrador de anuncios
de Facebook para Excel

3
00:00:06,686 --> 00:00:09,767
para extraer datos de rendimiento
de tus cuentas publicitarias.

4
00:00:11,267 --> 00:00:15,172
Para crear una plantilla de informe,
haz clic en “Administrar plantillas”.

5
00:00:16,307 --> 00:00:17,965
Haz clic en “Crear plantilla”

6
00:00:17,965 --> 00:00:21,280
y, luego, personaliza tu informe
según cómo quieras que aparezca

7
00:00:21,280 --> 00:00:24,051
en el administrador de anuncios
de Facebook para Excel.

8
00:00:24,051 --> 00:00:26,795
Debes pensar en los resultados
que más te interesan

9
00:00:26,795 --> 00:00:30,376
y, luego, estructurar tu plantilla
de informe de acuerdo con ellos.

10
00:00:31,922 --> 00:00:33,855
Haz clic en “Crear”.

11
00:00:33,855 --> 00:00:35,862
Después de crear la plantilla de informe,

12
00:00:35,862 --> 00:00:38,533
puedes descargar informes de
tus cuentas publicitarias.

13
00:00:40,417 --> 00:00:41,783
Para obtener más información sobre el

14
00:00:41,783 --> 00:00:43,545
administrador de anuncios
de Facebook para Excel,

15
00:00:43,545 --> 00:00:45,338
visita nuestras páginas de ayuda.

